# Springer Hello World #

## Build & Run ##

```sh
$ cd Springer_Hello_World
$ ./sbt
> container:start
> browse
```

If `browse` doesn't launch your browser, manually open [http://localhost:8080/](http://localhost:8080/) in your browser.

package com.springer.helloworld

import scalaj.http._

class MyScalatraServlet extends SpringerHelloWorldStack {

  get("/") {
    val url = getServletContext().getInitParameter("dateProvider") + "/date"
    val http_connTimeout = augmentString(getServletContext().getInitParameter("connTimeout")).toInt
    val http_readTimeout = augmentString(getServletContext().getInitParameter("readTimeout")).toInt

    val date = Http(url).option(HttpOptions.connTimeout(http_connTimeout)).option(HttpOptions.readTimeout(http_readTimeout)).asString

    <html>
      <body>
        <h1>Hello, world!</h1>
        <p>Say <a href="http://rd.springer.com">hello to Springer RD</a>.</p>
        <p>The current date is {date}</p>
      </body>
    </html>
  }
  
}
